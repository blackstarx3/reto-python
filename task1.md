# TASK1

```
Se necesita un script de python3 que haga lo siguiente
realice una peticion tipo GET a la siguiente URL https://63052287697408f7edc2669f.mockapi.io/client/zones
realizar un println() del resultado
leer una variable de entorno que se llame MI_IP_ACTUAL con valor 10.20.30.55
realizar una peticion tipo PUT a la URL https://63052287697408f7edc2669f.mockapi.io/client/zones/1
enviando como "body" el siguiente contenido:
```
```bash
{
    "content": "10.20.30.55"
}
```
```
el contenido debe ser ingresado dinamicamente con la variable de entorno
```