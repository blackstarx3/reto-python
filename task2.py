# TASK2


- Progresando con el ejercicio anterior, se necesita usar la siguiente variable:
```python
MIS_PAGINAS=[
    {'id':1,'ip':'10.20.33.44'},
    {'id':2,'ip':'192.168.1.23'},
    {'id':3,'ip':'120.12.15.47'}
    ]
```

- Donde lo que se necesita es iterar por cada objecto de la lista MIS_PAGINAS para realizar una peticion GET a `https://63052287697408f7edc2669f.mockapi.io/client/zones/[aqui va el id del primer objeto]` e imprimir en consola el resultado

- Luego se necesita que se vuelva a iterar en esta ocacion con una peticion tipo PUT para actualizar el valor de la IP usando los valores de la variable MIS_PAGINAS de manera correspondiente al `id`
